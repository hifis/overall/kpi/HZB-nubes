# Service Usage

* HZB nubes
    - CSV-File contains all userdata and HIFIS specific usage (e.g. number of Groupfolders)
    - KPi_nubes.csv is a legacy file, no Data will be published there
    - kpi-new.csv is the new place for current data
## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

| Data | Weighing |
| ----- | ----- |
| active user (last 24h) | |
| total Number of users | |
| files |  |
| shares |  |
| size Groupfolders | |
| number Groupfolders HZB | |
| number Groupfolders HIFIS | |
| number Groupfolders total | |

## Schedule

* daily (9:00 p.m)


